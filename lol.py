from lxml import etree
import os
import random

##  #   # ###   # ###  ##   ##   ##     #   #   # ###    ##  #   # #  ## ### ##
# # ## ## #  #  # #   #  # #     # #   # #  ##  # #  #  #  # ## ##   #   #   # #
##  # # # #   #   ### #  #  #    ##    ###  # # # #   # #  # # # # #  #  ### ##
#   #   # #  #  # #   #  #   #   # #  #   # #  ## #  #  #  # #   # #   # #   # #
#   #   # ###   # ###  ##  ##    #  # #   # #   # ###    ##  #   # # ##  ### #  #

_character = ["_",",","-","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","v","w","x","y","z"]
_anim = ["0x0","0x1","0x2","0x3","0x4","0x5","0x6","0x7","0x8","0x9"]
_direction = ["Right","DownRight","Down","DownLeft","Left","UpLeft","Up","UpRight"]
_facemode = ["Standard","AbsCoord","AbsCoordFaceL","AbsCoordFaceR","Bottom_C_FaceR","Bottom_L_FaceInw","Bottom_R_FaceInw","Bottom_L_Center","Bottom_R_Center","Bottom_C_FaceL","Bottom_L_FaceOutw","Bottom_R_FaceOutw","Bottom_LC_FaceOutw","Bottom_RC_FaceOutw","Top_C_FaceR","Top_L_FaceInw","Top_R_FaceInw","Top_L_Center","Top_R_Center","Top_C_FaceL","Top_L_FaceOutw","Top_RC_FaceR","Top_LC_FaceOutw","Top_RC_FaceOutw"]
_facetype = ["NORMAL","HAPPY","PAIN","ANGRY","WORRIED","SAD","CRYING","SHOUTING","TEARY-EYED","DETERMINED","JOYOUS","INSPIRED","SURPRISED","DIZZY","SPECIAL0","SPECIAL1","SIGH","STUNNED","SPECIAL2","SPECIAL3"]
_actorid = []
_fichierData = "pmd2scriptdata.xml"
# IDEA: add a coherent thing, eg always replace pain for partner with sad, ost 34 by 53, the word hello by afj_i...

#liste des options :
#   "randomAnimation" randomize les animation dans l'overworld
#   "randomMusic" randomize les musique dans l'overworld
#   "randomText" randomize le texte de l'overworld ( sauf les espaces )
#   "randomFace" randomize les visage
#       option : todo
#    "randomMove" randomize les déplacement
def selectList(liste):
    if len(liste) != 0:
        if len(liste) == 1:
            return liste[0]
        else:
            return liste[random.randrange(0,len(liste)-1)]
    return False

def removeXML(arbre,toremove):
    for remove in toremove:
        arbre.remove(remove)

def randomAnimation(arbre):
    allAnimid = arbre.iter("SetAnimation")
    for animid in allAnimid:
        animid.attrib["animid"]=_anim[random.randrange(0,len(_anim)-1)]

    allWait = arbre.findall("WaitExecuteLives")
    removeXML(arbre,allWait)

    allLives = arbre.findall("lives")
    for lives in allLives:
        allWait2 = lives.findall("WaitAnimation")
        removeXML(lives,allWait2)
        if len(lives)==0:
            command = etree.SubElement(lives,"Wait")
            command.attrib["duration"] = "10"

    allObject = arbre.findall("object")
    for objects in allObject:
        allWait2 = objects.findall("WaitAnimation")
        removeXML(objects, allWait2)
        if len(objects) == 0:
            command = etree.SubElement(objects,"Wait")
            command.attrib["duration"] = "10"

def randomMusic(arbre):
    allBGM = arbre.findall("bgm_PlayFadeIn")
    for BGM in allBGM:
        BGM.attrib["bgm"] = str(random.randint(0,150))

def randomizeText(texteO):
    texte = ""
    if type(texteO) != str:
        return ""
    for loop in texteO:
        if loop!=" ":#on change
            texte = texte+_character[random.randrange(0,len(_character)-1)]
        else:
            texte = texte+loop
    return texte

def randomText(arbre):
    allString = arbre.iter("String")
    for String in allString:
        String.text = randomizeText(String.text)

def randomTurn(arbre,root):
    ############################
    ### random turn in lives ###
    ############################
    allLives = arbre.findall("lives")
    for lives in allLives:
        allTurn = lives.findall("Turn2Direction")
        for Turn in allTurn:
            Turn.attrib["direction"] = selectList(_direction)

    #######################
    ### pre random turn ###
    #######################
    for ScriptSet in root.findall("ScriptSet"):
        for ScriptData in ScriptSet.findall("ScriptData"):
            for Layers in ScriptData.findall("Layers"):
                for Layer in Layers.findall("Layer"):
                    for directioner in Layer:
                        tag = directioner.tag
                        if tag in ["Objects","Actors"]:
                            for directione in directioner:
                                tagD = directione.tag
                                if tagD in ["Object","Actor"]:
                                    directione.attrib["facing"] = selectList(_direction)

def randomFace(arbre, root = None, modeFace = True, Face = True, proFace = True):
    """mélange les image des visage
    modeFace : change la position du visage
    Face : change l'expression du visage
    proFace : change le propriétaire du visage"""
    # TODO: before every message

    if proFace:
        if root == None:
            raise
        #get all possible face
        possible = []
        for actors in root.iter("Actors"):
            for actor in actors.findall("Actor"):
                possible.append(actor.attrib["actorid"])

    allMessageSetFace = arbre.findall("message_SetFace")
    for message_SetFace in allMessageSetFace:
        if modeFace:
            message_SetFace.attrib["facemode"] = selectList(_facemode)
        if Face:
            message_SetFace.attrib["face"] = selectList(_facetype)
        if proFace:# TODO: ensure that the remplaced visage exist ( kaomado ? )
            newActorID = selectList(possible)
            if newActorID != False:
                message_SetFace.attrib["actorid"] = newActorID


def randomMove(arbre,interval = 7):
    """randomize les déplacement des personnage / object
    interval : la différence de mouvement ( attention, 10, c'est déja beaucoup )"""
    # TODO: seem to affect the camera. make the camera randomization optional
    allMovePositionMark = arbre.iter("MovePositionMark")
    for MovePositionMark in allMovePositionMark:
        if "x" in MovePositionMark.attrib:
            MovePositionMark.attrib["x"] = str(int(MovePositionMark.attrib["x"])+random.randint(0-interval,interval))
            MovePositionMark.attrib["y"] = str(int(MovePositionMark.attrib["y"])+random.randint(0-interval,interval))

def randomActorInit():
    #lecture du fichier
    fichierStream = open(_fichierData,"r",encoding="1252")
    fichierLu = fichierStream.read()
    fichierStream.close()
    #lecture du xml
    root = etree.fromstring(fichierLu)
    game = root.find("ScriptData").findall("Game")[2]
    for entity in game.iter("Entity"):
        truc = entity.attrib["name"]
        if avance == "NPC" or avance == "PLAYER" or avance == "ATTENDANT" or avance == "ATTENDANT1" or avance == "ATTENDANT2":
            _actorid.append(truc)

    print("ok")

def randomActor(root):
    # TODO: le faire plus vivable ( svp, bougez les personnage ( ou juste mélanger les fichier de sprite ))
    for actors in root.iter("Actors"):
        for actor in actors.findall("Actor"):
            actor.attrib["actorid"] = selectList(_actorid)

def randomStartPos(root,interval=1):
    for Layers in root.iter("Layers"):
        for Layer in Layers.findall("Layer"):
            for actors in Layer:
                for actor in actors:
                    tag = actor.tag
                    if tag in ["Actor","Objects"]:
                        actor.attrib["x"] = str(int(actor.attrib["x"])+random.randint(0-interval,interval))
                        actor.attrib["y"] = str(int(actor.attrib["y"])+random.randint(0-interval,interval))

def process(fichier, name, ou):
    print(fichier)
    #lecture du fichier
    fichierStream = open(fichier,"r",encoding="1252")
    fichierLu = fichierStream.read()
    fichierStream.close()
    #lecture du xml
    root = etree.fromstring(fichierLu)
    ##################
    ### processing ###
    ##################

    #finding all script element
    allScriptSet = root.findall("ScriptSet")
    for ScriptSet in allScriptSet:
        allScriptSequence = ScriptSet.findall("ScriptSequence")
        for ScriptSequence in allScriptSequence:
            Code = ScriptSequence.find("Code")
            allFunction = Code.findall("Function")
            for Function in allFunction:
                ######################################
                ### processing pour tout le script ###
                ######################################
                #randomActor(root)
                randomStartPos(root)
                randomMusic(Function)
                randomText(Function)
                randomAnimation(Function)
                randomTurn(Function, root)
                randomFace(Function, root, modeFace = True, Face = True, proFace = True)
                randomMove(Function)

    a = open(ou+"/"+name,"wb")
    a.write(b"<?xml version=\"1.0\"?>\n"+etree.tostring(root))
    a.close()

def processAll(folder, ou):
    for loop in os.listdir(folder):
        process(folder+"/"+loop, loop, ou)

try:
    os.mkdir("modified")
except:
    pass

try:
    os.mkdir("modified/scripts")
except:
    pass
#randomActorInit()
#print(randomizeText(input()))
processAll("export/scripts","modified/scripts")
#process("export/scripts/D19P11A.xml", "D19P11A.xml", "funny")
